import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:vgchicken_example1/page2.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.amber,
        textTheme: TextTheme(
          title: TextStyle(
            fontSize: 38,
            color: Color(0xFF282642),
            fontWeight: FontWeight.w500,
            letterSpacing: 2,
          ),
          subtitle: TextStyle(
            fontSize: 20,
            color: Color(0xFF282642),
            fontWeight: FontWeight.w800,
            letterSpacing: 2,
          ),
        ),
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedTab = 0;

  _onTabBarTap(int index) {
    setState(() {
      _selectedTab = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: _appBar(),
        body: Container(
          color: Colors.white,
          child: ListView(
            children: <Widget>[
              _title(),
              _subTitle(),
              _horizontalList(),
              _bodyTitle(),
              _verticalList(),
            ],
          ),
        ),
        bottomNavigationBar: _bottomNavigation());
  }

  Widget _appBar() => AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: Icon(
          Icons.menu,
          color: Color(0xFF282642),
        ),
        title: Text(
          'Bookstores',
          style: TextStyle(
              color: Color(0xFF282642),
              fontWeight: FontWeight.w600,
              letterSpacing: 1),
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: Icon(
              Icons.search,
              color: Color(0xFF282642),
            ),
          ),
        ],
      );

  Widget _title() => Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Find',
              style: Theme.of(context).textTheme.title,
            ),
            Text(
              'Beautiful World',
              style: Theme.of(context).textTheme.title,
            ),
          ],
        ),
      );

  Widget _subTitle() => Center(
        child: Container(
          decoration: BoxDecoration(
            border: Border(
              bottom: BorderSide(
                color: Colors.amber[100],
                width: 5,
              ),
            ),
          ),
          padding: EdgeInsets.only(top: 30, bottom: 5),
          child: Text(
            'Discover',
            style: Theme.of(context).textTheme.subtitle,
          ),
        ),
      );

  Widget _horizontalList() => Container(
        height: 250,
        child: ListView(
          padding: EdgeInsets.only(top: 20, left: 20, right: 20),
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          children: <Widget>[
            _horizontalListItem(
                'assets/images/pic1.jpg', 'Porto Santo', 'Portugal'),
            _horizontalListItem(
                'assets/images/pic2.jpg', 'Ned\'s Beach', 'Australia'),
            _horizontalListItem(
                'assets/images/pic3.jpg', 'Hao Wan Jiao', 'Taipei'),
            _horizontalListItem(
                'assets/images/pic4.jpg', 'Barcelo Beach', 'Spain'),
          ],
        ),
      );

  Widget _horizontalListItem(
          String imgUrl, String placeName, String countryName) =>
      Row(
        children: <Widget>[
          InkWell(
            onTap: () => {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Page2(placeName, imgUrl),
                ),
              )
            },
            child: Card(
              elevation: 3,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Stack(
                children: <Widget>[
                  Container(
                    width: 160,
                    height: 250,
                    child: Hero(
                      tag: placeName,
                      child: Image.asset(
                        imgUrl,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          Colors.transparent,
                          Colors.black38,
                        ],
                      ),
                    ),
                    width: 160,
                    height: 250,
                  ),
                  Positioned(
                    bottom: 33,
                    left: 15,
                    child: Text(
                      placeName,
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          fontSize: 18,
                          letterSpacing: 1),
                    ),
                  ),
                  Positioned(
                    bottom: 15,
                    left: 15,
                    child: Row(
                      children: <Widget>[
                        Icon(
                          FontAwesomeIcons.solidPaperPlane,
                          size: 7,
                          color: Colors.white,
                        ),
                        SizedBox(width: 5),
                        Text(
                          countryName,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                              letterSpacing: 1),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(width: 10)
        ],
      );

  Widget _bodyTitle() => Padding(
        padding: EdgeInsets.only(left: 36, top: 36),
        child: Text(
          'Discover',
          style: Theme.of(context).textTheme.subtitle,
        ),
      );

  Widget _verticalList() => Padding(
      padding: EdgeInsets.only(left: 25, right: 25, top: 15),
      child: Column(
        children: <Widget>[
          _verticalListItem(
            'assets/images/boat1.jpg',
            'Delft\'s Impression',
            'Western Netherlands',
            4,
            130,
            190,
          ),
          _verticalListItem(
            'assets/images/boat2.jpg',
            'GB Tech Shit',
            'New Taipei',
            1,
            120,
            360,
          ),
          _verticalListItem(
            'assets/images/boat3.jpg',
            'VG Chicken Studio',
            'Taiwan Taipei',
            5,
            360,
            1000,
          ),
        ],
      ));

  Widget _verticalListItem(
    String imgUrl,
    String title,
    String subtitle,
    int star,
    int salePrice,
    int originalPrice,
  ) =>
      Column(
        children: <Widget>[
          Container(
            height: 130,
            decoration: BoxDecoration(
              color: Color(0xffe2e7ef),
              borderRadius: BorderRadius.circular(20),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: Image.asset(
                      imgUrl,
                      width: 90,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 35, left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        title,
                        style: TextStyle(
                          color: Color(0xFF282642),
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      Text(
                        subtitle,
                        style: TextStyle(
                          color: Colors.grey[500],
                          fontSize: 13,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Row(
                        children: _getStars(star),
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            '\$$salePrice',
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                              color: Colors.red,
                              letterSpacing: 1,
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            '\$$originalPrice',
                            style: TextStyle(
                              decoration: TextDecoration.lineThrough,
                              fontSize: 14,
                              color: Colors.grey,
                              letterSpacing: 1,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 20)
        ],
      );

  List<Widget> _getStars(int numbers) {
    List<Widget> resultList = [];

    for (int i = 0; i < numbers; i++) {
      resultList.add(_star(Colors.amber));
    }
    for (int i = 0; i < 5 - numbers; i++) {
      resultList.add(_star(Colors.grey[400]));
    }
    return resultList;
  }

  Widget _star(Color color) => Padding(
        padding: EdgeInsets.only(right: 3),
        child: Icon(
          Icons.star,
          color: color,
          size: 16,
        ),
      );

  Widget _bottomNavigation() => Container(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              spreadRadius: 5.0,
              blurRadius: 20.0,
              color: Colors.black54,
              offset: Offset(0.0, 15.0),
            ),
          ],
        ),
        child: BottomNavigationBar(
          unselectedItemColor: Colors.grey[400],
          selectedItemColor: Colors.black,
          showUnselectedLabels: false,
          onTap: _onTabBarTap,
          currentIndex: _selectedTab,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Icon(
                FontAwesomeIcons.solidCircle,
                size: 4,
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(CupertinoIcons.car),
              title: Icon(
                FontAwesomeIcons.solidCircle,
                size: 4,
              ),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              title: Icon(
                FontAwesomeIcons.solidCircle,
                size: 4,
              ),
            ),
          ],
        ),
      );
}

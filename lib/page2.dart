import 'package:flutter/material.dart';

class Page2 extends StatefulWidget {
  final String title;
  final String imgUrl;

  const Page2(this.title, this.imgUrl);

  @override
  State<Page2> createState() => Page2State();
}

class Page2State extends State<Page2> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Hero(
        tag: widget.title,
        child: Image.asset(
          widget.imgUrl,
          fit: BoxFit.cover,
          height: 400,
          width: 100,
        ),
      ),
    );
  }
}
